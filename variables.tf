variable "name" {}
variable "policy" {}
variable "kiam_server_arn" {}
variable tags {
  type = map
}
