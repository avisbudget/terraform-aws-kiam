# Create a role that can be assumed by kiam server.
resource "aws_iam_role" "main" {
  name               = "${var.name}-kiam"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {"Service": "ec2.amazonaws.com"},
      "Effect": "Allow"
    },
    {
      "Action": "sts:AssumeRole",
      "Principal": {"AWS": "${var.kiam_server_arn}"},
      "Effect": "Allow"
    }
  ]
}
EOF
  tags = merge(var.tags, {
    Name = "${var.name}-kiam"
  })
  lifecycle {
    ignore_changes = [tags]
  }
}

# Attach our desired policy.
resource "aws_iam_role_policy" "main" {
  name   = var.name
  role   = aws_iam_role.main.id
  policy = var.policy
}
